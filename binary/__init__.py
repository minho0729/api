try:
    from serialize import BinarySerialize
    from deserialize import BinaryDeserialize
except ImportError:
    from .serialize import BinarySerialize
    from .deserialize import BinaryDeserialize


def serialize(a):
    return BinarySerialize(a)()


def deserialize(a):
    return BinaryDeserialize(a)()
