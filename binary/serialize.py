import os
import sys
import json
import struct


class BinarySerialize:

    def __init__(self, a):
        self.a = a

    def __call__(self, a=Ellipsis):
        if a is Ellipsis:
            a = self.a

        if a is None:
            return b'\xC0'

        if a is False:
            return b'\xC2'

        if a is True:
            return b'\xC3'

        if isinstance(a, int):
            if a in range(1 << 7):
                return self._uint(a, 1)
            if a in range(1 << 8):
                return b'\xCC' + self._uint(a, 1)
            if a in range(1 << 16):
                return b'\xCD' + self._uint(a, 2)
            if a in range(1 << 32):
                return b'\xCE' + self._uint(a, 3)
            if a in range(1 << 64):
                return b'\xCF' + self._uint(a, 4)

        if isinstance(a, float):
            return b'\xCA' + self._float(a)

        if isinstance(a, str):
            s = self._str(a)
            l = len(s)
            if l in range(1 << 5):
                return self._uint(l + 0xA0, 1) + s
            if l in range(1 << 8):
                return b'\xD9' + self._uint(l, 1) + s
            if l in range(1 << 16):
                return b'\xDA' + self._uint(l, 2) + s

        if isinstance(a, list):
            s = self._list(a)
            l = len(a)
            if l in range(1 << 4):
                return self._uint(l + 0x90, 1) + s
            if l in range(1 << 16):
                return b'\xDC' + self._uint(l, 2) + s

        if isinstance(a, dict):
            s = self._dict(a)
            l = len(a)
            if l in range(1 << 4):
                return self._uint(l + 0x80, 1) + s
            if l in range(1 << 16):
                return b'\xDE' + self._uint(l, 2) + s

        print(a, self.a)
        raise NotImplementedError()

    def _uint(self, a, size):
        if size == 1:
            return struct.pack('>B', a)
        if size == 2:
            return struct.pack('>H', a)
        if size == 3:
            return struct.pack('>L', a)
        if size == 4:
            return struct.pack('>Q', a)

        raise NotImplementedError()

    def _float(self, a):
        return struct.pack('>f', a)

    def _str(self, a):
        return a.encode()

    def _list(self, a):
        return b''.join(
            self(x) for x in a
        )

    def _dict(self, a):
        return b''.join(
            b''.join((self(k), self(v))) for k, v in a.items()
        )


if __name__ == '__main__':
    file_name = sys.argv[1] if len(sys.argv) > 1 else 'test.json'
    with open(file_name, 'r', encoding='utf-8') as f:
        a = json.load(f)

    b = BinarySerialize(a)()
    with open(os.path.splitext(file_name)[0] + '.bin', 'wb') as f:
        f.write(b)
