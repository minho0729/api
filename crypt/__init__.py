import os
from Crypto.Cipher import AES


KEY = os.getenv('PJSEKAI_KEY', '').encode()
IV = os.getenv('PJSEKAI_IV', '').encode()
JWT = os.getenv('PJSEKAI_JWT', '').encode()


def crypt():
    return AES.new(KEY, AES.MODE_CBC, IV)


def encrypt(data):
    pad = 16 - len(data) % 16
    pad = pad.to_bytes(1, 'big') * pad
    return crypt().encrypt(data + pad)


def decrypt(data):
    data = crypt().decrypt(data)
    pad = data[-1]
    return data[:-pad]
