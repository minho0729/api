import time
import functools

import requests

from .sekai import Sekai


class Assetbundle:

    domain = 'https://assetbundle.sekai.colorfulpalette.org'

    @functools.lru_cache(maxsize=1)
    def __new__(cls, *args, **kwargs):
        return super().__new__(cls)

    @functools.lru_cache(maxsize=1)
    def __init__(self):
        self.sekai = Sekai.new()

    def __repr__(self):
        return 'Assetbundle(%s)' % self.sekai.asset_version

    def get_url(self, name):
        return '%s/%s/%s/ios/%s?t=%s' % (
            self.domain,
            self.sekai.asset_version,
            self.sekai.asset_hash,
            name,
            time.strftime("%Y%m%d%H%M%S", time.localtime())
        )

    def get(self, name):
        data = requests.get(self.get_url(name)).content
        data = bytearray(data)
        data[0:4] = []
        for i in range(128):
            if i % 8 < 5:
                data[i] = 255 - data[i]
        return bytes(data)
