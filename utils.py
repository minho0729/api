import json
import time
import functools


@functools.lru_cache()
def database(name, *, key=None):
    with open('master/%s.json' % name, encoding='utf-8') as f:
        a = json.load(f)
    if key is None:
        return a
    return {
        item[key]: item
        for item in a
    }


@functools.lru_cache()
def get_gacha_behavior_id(cost_resource_quantity=3000, cost_resource_type='jewel'):
    t = time.time() * 1000
    gacha_behavior = None
    for gacha in database('gachas'):
        if not gacha['startAt'] < t < gacha['endAt']:
            continue
        for behavior in gacha['gachaBehaviors']:
            if (
                behavior['costResourceType'] == cost_resource_type and
                behavior['costResourceQuantity'] == cost_resource_quantity and
                not gacha_behavior
            ):
                gacha_behavior = behavior

    return gacha_behavior['gachaId'], gacha_behavior['id']


def update(sekai, a):
    try:
        sekai['userGachas']
    except KeyError:
        pass
    else:
        a['gachaTimes'] = 0
        for user_gacha in sekai['userGachas']:
            gacha = database('gachas', key='id')[user_gacha['gachaId']]
            gacha_behavior = None
            for gacha_behavior in gacha['gachaBehaviors']:
                if gacha_behavior['id'] == user_gacha['gachaBehaviorId']:
                    break
            a['gachaTimes'] += gacha_behavior['spinCount'] * user_gacha['count']

    try:
        a['jewel'] = sekai['user']['userGamedata']['chargedCurrency']['free']
    except KeyError:
        pass
    else:
        pass

    try:
        sekai['userCards']
    except KeyError:
        pass
    else:
        a['cardIDs'] = [card['cardId'] for card in sekai['userCards']]
        a['cardIDs'] = [card_id for card_id in a['cardIDs'] if database('cards', key='id')[card_id]['rarity'] >= 3]
        a['card4'] = len([card_id for card_id in a['cardIDs'] if database('cards', key='id')[card_id]['rarity'] == 4])
        a['card3'] = len([card_id for card_id in a['cardIDs'] if database('cards', key='id')[card_id]['rarity'] == 3])

    try:
        sekai['userMaterials']
    except KeyError:
        pass
    else:
        a['materials'] = {}
        for material in sekai['userMaterials']:
            a['materials'][str(material['materialId'])] = material['quantity']

    return a
