import re
import time
import uuid
import random
import functools

import jwt  # pip install pyjwt
import requests

import api.crypt
import api.binary


def request_retry(retry=10):
    def decorator(f):
        @functools.wraps(f)
        def wrapper(*args, **kwargs):
            t = 1.0
            exception = None
            for _ in range(retry):
                try:
                    return f(*args, **kwargs)
                except requests.exceptions.RequestException as e:
                    time.sleep(t)
                    t *= 1.5
                    exception = e
            else:
                raise exception
        return wrapper
    return decorator


class Sekai:

    app_version = '1.8.1'
    domains = {
        'debug': 'debug-game-api-fkgp64xt5r333dpdrw4w5axcg764x8n5.sekai.colorfulpalette.org',
        'develop': '{0}-game-api-fkgp64xt5r333dpdrw4w5axcg764x8n5.sekai.colorfulpalette.org',
        'staging': 'staging-game-api-fkgp64xt5r333dpdrw4w5axcg764x8n5.sekai.colorfulpalette.org',
        'production': 'production-game-api.sekai.colorfulpalette.org',
        'promotion': '{0}-game-api-fkgp64xt5r333dpdrw4w5axcg764x8n5.sekai.colorfulpalette.org',
    }

    def __init__(self, *, server='production'):
        '''
        To create Sekai instances, please use one of the following constructors:
          * `Sekai.new()`
          * `Sekai.from_credential(user_id, credential)`
          * `Sekai.from_inherit(inherit_id, inherit_password)`
        '''

        self.domain = self.domains[server]

        self.asset_hash = None
        self.asset_version = None
        self.data_version = None

        self.user_id = None
        self.credential = None

        self.session_token = None

        self.install_id = str(uuid.uuid4())

        self._ = {}
        self.__ = {}

    def __repr__(self):
        return 'Sekai(%s)' % self.user_id

    def __getitem__(self, index):
        return self.__[index]

    def __contains__(self, index):
        return index in self.__

    def keys(self):
        return self.__.keys()

    def user_auth(self):
        self('PUT', '/api/user/{user_id}/auth?refreshUpdatedResources=False', {
            'credential': self.credential,
        })
        self.asset_hash = self._['assetHash']
        self.asset_version = self._['assetVersion']
        self.data_version = self._['dataVersion']
        self.session_token = self._['sessionToken']

    @staticmethod
    def new(*, server='production'):
        self = Sekai(server=server)
        self('POST', '/api/user', {
            'platform': 'iOS',
            'deviceModel': 'iPad7,5',
            'operatingSystem': 'iOS 14.0',
        })
        self.user_id = self._['userRegistration']['userId']
        self.credential = self._['credential']
        self.user_auth()
        return self

    @staticmethod
    def from_credential(user_id, credential, *, server='production'):
        self = Sekai(server=server)
        self.user_id = user_id
        self.credential = credential
        self.user_auth()
        return self

    @staticmethod
    def from_inherit(inherit_id, inherit_password, *, server='production'):
        self = Sekai(server=server)
        self('POST', '/api/inherit/user/%s?isExecuteInherit=False' % inherit_id, headers={
            'x-inherit-id-verify-token': jwt.encode({
                'inheritId': inherit_id,
                'password': inherit_password,
            }, key=api.crypt.JWT, algorithm='HS256')
        })
        self('POST', '/api/inherit/user/%s?isExecuteInherit=True' % inherit_id, headers={
            'x-inherit-id-verify-token': jwt.encode({
                'inheritId': inherit_id,
                'password': inherit_password,
            }, key=api.crypt.JWT, algorithm='HS256')
        })
        self.user_id = self._['afterUserGamedata']['userId']
        self.credential = self._['credential']
        self.user_auth()
        return self

    @property
    def headers(self):
        return {
            'x-ai': '',
            'x-ga': '',
            'x-ma': '',
            'x-kc': 'c09bcca0-6688-440e-996d-acb64643b880',
            'x-if': '00000000-0000-0000-0000-000000000000',

            'x-devicemodel': 'iPad7,5',
            'x-operatingsystem': 'iOS 14.0',
            'x-platform': 'iOS',
            'user-agent': 'pjsekai/23 CFNetwork/1197 Darwin/20.0.0',

            'x-unity-version': '2019.4.3f1',
            'x-app-version': self.app_version,
            'x-asset-version': self.asset_version,
            'x-data-version': self.data_version,

            'x-session-token': self.session_token,
            'x-install-id': self.install_id,
            'x-request-id': str(uuid.uuid4()),

            'accept': 'application/octet-stream',
            'content-type': 'application/octet-stream',
        }

    @request_retry(retry=10)
    def __call__(self, method, url, data=..., headers=None):
        if url.startswith('/'):
            url = 'https://' + self.domain + url
        url = re.sub('{(.+?)}', lambda match: str(self.__getattribute__(match.groups()[0])), url)

        if data is ...:
            data = None
        else:
            data = api.crypt.encrypt(api.binary.serialize(data))

        headers = {
            key: value
            for key, value in {**self.headers, **(headers or {})}.items()
            if value is not None
        }

        r = requests.request(method=method, url=url, data=data, headers=headers)
        status_code = r.status_code

        try:
            if r.headers.get('x-session-token'):
                self.session_token = r.headers.get('x-session-token')

            r = r.content
            r = api.crypt.decrypt(r)
            r = api.binary.deserialize(r)

            assert status_code == 200

        except:
            raise AssertionError(status_code, r)

        self._ = r
        if 'updatedResources' in r:
            for key, value in r['updatedResources'].items():
                self.__[key] = value

        return r

    def suite(self) -> None:
        self.__ = self('GET', '/api/suite/user/{user_id}')

    def tutorials(self) -> None:
        self('PATCH', '/api/user/{user_id}/tutorial', {'tutorialStatus': 'opening_1'})
        self('PATCH', '/api/user/{user_id}', {'userGamedata': {'name': 'セカイの住人'}})

        self('PATCH', '/api/user/{user_id}/tutorial', {'tutorialStatus': 'gameplay'})
        self('PATCH', '/api/user/{user_id}/tutorial', {'tutorialStatus': 'opening_2'})
        self('PATCH', '/api/user/{user_id}/tutorial', {'tutorialStatus': 'unit_select'})
        self('PATCH', '/api/user/{user_id}/tutorial', {'tutorialStatus': 'theme_park_opening'})
        self('PATCH', '/api/user/{user_id}/tutorial', {'tutorialStatus': 'summary'})
        self('PATCH', '/api/user/{user_id}/tutorial', {'tutorialStatus': 'end'})

        # self('POST', '/api/user/{user_id}/story/unit_story/episode/20000', None)
        self('POST', '/api/user/{user_id}/story/unit_story/episode/30000', None)
        self('POST', '/api/user/{user_id}/story/unit_story/episode/40000', None)
        self('POST', '/api/user/{user_id}/story/unit_story/episode/50000', None)
        self('POST', '/api/user/{user_id}/story/unit_story/episode/60000', None)

    def inherit(self, password: str) -> str:
        return self('PUT', '/api/user/{user_id}/inherit', {'password': password})['userInherit']['inheritId']

    def presents(self) -> None:
        self('POST', '/api/user/{user_id}/present', {
            'presentIds': [present['presentId'] for present in self['userPresents']],
        })

    def beginner_missions(self) -> None:
        self('PUT', '/api/user/{user_id}/mission/beginner_mission', {
            'missionIds': [
                mission['missionId']
                for mission in self['userMissionStatuses']
                if mission['missionType'] == 'beginner_mission' and mission['missionStatus'] == 'achieved'
            ]
        })

    def gacha(self, gacha_id, gacha_behavior_id) -> None:
        self('PUT', '/api/user/{user_id}/gacha/%s/gachaBehaviorId/%s' % (gacha_id, gacha_behavior_id), None)

    def login_bonus(self) -> None:
        self('PUT', '/api/user/{user_id}/home/refresh', {'refreshableTypes': ['login_bonus']})

    def card_exchange(self) -> None:
        cards = [
            {**card, 'duplicateCount': 0}
            for card in self['userCards']
            if card['duplicateCount'] != 0
        ]

        if not cards:
            return

        self('PUT', '/api/user/{user_id}/card/?behavior=exchange', {
            'userCards': cards
        })

    def exchange_gacha_ceil_item(self) -> None:
        # gacha_ceil_exchange_ids = []
        gacha_ceil_exchange_requests = []
        gacha_ceil_exchange_summaries = api.database('gachaCeilExchangeSummaries', key='gachaCeilItemId')
        for gacha_ceil_item in self['userGachaCeilItems']:
            gacha_ceil_exchange_summary = gacha_ceil_exchange_summaries[gacha_ceil_item['gachaCeilItemId']]
            if not gacha_ceil_exchange_summary['startAt'] < time.time() * 1000 < gacha_ceil_exchange_summary['endAt']:
                continue
            for gacha_ceil_exchange in gacha_ceil_exchange_summary['gachaCeilExchanges']:
                if (
                    gacha_ceil_exchange['gachaCeilExchangeCost']['quantity'] == 10 and
                    gacha_ceil_exchange['gachaCeilExchangeCost']['resourceType'] == 'gacha_ceil_item'
                ):
                    # gacha_ceil_exchange_ids += [gacha_ceil_exchange['id']] * (gacha_ceil_item['quantity'] // 10)
                    gacha_ceil_exchange_requests.append({
                        'gachaExchangeId': gacha_ceil_exchange['id'],
                        'exchangeCount': gacha_ceil_item['quantity'] // 10,
                        'gachaCeilExchangeSubstituteCostId': 0,
                        'substituteCostCount': 0,
                    })

        # if not gacha_ceil_exchange_ids:
        #     return

        # self('PUT', '/api/user/{user_id}/exchange/gacha-ceil-item', {
        #     'gachaCeilExchangeIds': gacha_ceil_exchange_ids
        # })

        for gacha_ceil_exchange_request in gacha_ceil_exchange_requests:
            self('PUT', '/api/user/{user_id}/exchange/gacha-ceil-item', {
                'gachaCeilExchangeIds': None,
                'gachaCeilExchangeRequest': gacha_ceil_exchange_request,
            })
